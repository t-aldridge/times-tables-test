# Times tables quiz – Written by Teymour Aldridge.
# Watch out, any attempt at subterfuge may cause this program to explode. It has done so previously (trust me).
# Import the tkinter (GUI generation) module
import tkinter
# Import the random number generator package (from the random module)
from random import randint
#Ensure that the two numbers have a start value (they get updated later in the program – lines)
number1 = randint(1,12)
number2 = randint(1,12)
#set up the score Variables
score = 0
# Create the window
window = tkinter.Tk()
#validation function
def Validate():
    # Clear the entry form
    #ensure that the variables in the function are global not local
    global score
    global q
    global number1
    global number2
    # Get the answer from the entry form
    answer_string = answer.get()
    #attempt to convert it to an integer
    try:
        answer_value = int(answer_string)
    #if that fails.... PANIC!!!
    except ValueError as e:
        print(e)
        return
    #compare the user's answer to the actual one (of course the computer gets it right)
    if answer_value == number1 * number2:
    #add a point
        score += 1
    #tell the user they got it right
        print('True')
    #change the values for the next question
        number1 = randint(1,12)
        number2 = randint(1,12)
    #update the lables and score counter
        q.configure(text="What is " + str(number1) + "x" + str(number2) + " ?")
        scorecounter.configure(text="Score: " + str(score))
        answer.delete(0, 'end')
    else:
    #minus a point and allow the user another try
        score -= 1
        print('False')
        answer.delete(0, 'end')
i=0
while i==0:
    # Variables that store the score and question values
    qtext=str("What is " + str(number1) + "x" + str(number2) + " ?")
    scoretext=str("Score: " + str(score))
    # Create the question (put the lable in tkinter window)
    q = tkinter.Label(window, text=qtext)
    # Pack q
    q.pack()
    # Set up the answer form
    answer = tkinter.Entry(window)
    # Set up the submit button
    submit = tkinter.Button(window, text = "Submit", command=Validate)
    # set up the score counter
    scorecounter = tkinter.Button(window, text=scoretext)
    #pack the entry form, submission form and scorecounter button.
    answer.pack()
    submit.pack()
    scorecounter.pack()
    #start the window mainloop()
    window.mainloop()
