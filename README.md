### What is this repository for? ###

* A simple times table testing app.

### How do I get set up? ###

* 1) Download the file. 
* 2) Open the file in python 3.
* 3) Run the file

### Contribution guidelines ###

* To contribute something drop me a line (me@teymour.tk)

### Who do I talk to? ###

* For issues talk to me@teymour.tk

This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 International License. (View a human readable summary at https://creativecommons.org/licenses/by-sa/4.0/)